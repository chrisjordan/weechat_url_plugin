################################################################################
# 
# The MIT License (MIT)
# 
# Copyright (c) 2014 Chris Jordan <chris@chrisjordan.io>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# 
################################################################################
import weechat
import re
import string
import os
import webbrowser

name = "url"
author = "Chris Jordan <chris@chrisjordan.io>"
version = "0.1.0"
license = "MIT"
description = "Automatically adds id's to urls in chat messages and opens them in your webbrowser with the /u <id> command"
shutdown_function = ""
charset = ""

weechat.register(name, author, version, license, description, shutdown_function, charset)

url_re = re.compile(r"https?://\S+")
current_id = 0
max_urls = 1000
url_list = [""] * max_urls

def mod_privmsg(data, modifier, modifier_data, message):
    global current_id
    global max_urls
    matches = url_re.findall(message)
    if matches:
        for match in matches:
            message = string.replace(message, match, "%s:%s" % (current_id, match), 1)
            url_list[current_id] = match
            current_id = (current_id + 1) % max_urls
    return message

weechat.hook_modifier("irc_in_privmsg", "mod_privmsg", "")

def print_url_list():
    global max_urls
    global url_list
    for url_id in range(0, max_urls):
        if url_list[url_id] == "":
            break
        weechat.prnt("", "%s: %s" % (url_id, url_list[url_id]))

def urls_hook_callback(data, buffer, args):
    global url_list
    if args == "urls":
        print_url_list()
    else:
        try:
            int(args)
        except ValueError:
            weechat.prnt("", "Error: invalid argument passed to /u")
            return weechat.WEECHAT_RC_OK
        url_id = int(args)
        if url_id < max_urls:
            savout = os.dup(1)
            os.close(1)
            os.open(os.devnull, os.O_RDWR)
            try:
                webbrowser.open(url_list[url_id], 2, True)
            finally:
                os.dup2(savout, 1)
        else:
            weechat.prnt("", "Error: value larger than max_urls passed to /u")
    return weechat.WEECHAT_RC_OK

hook_command = "u"
hook_description = "Opens urls in webbrowser based on an id"
hook_args = "[urls] | [id]"
hook_args_description = "urls: list urls stored\nid: open id's url in your browser"
hook_completion = "urls"
hook_callback = "urls_hook_callback"
hook_callback_data = ""
hook = weechat.hook_command(hook_command, hook_description, hook_args, hook_args_description, hook_completion, hook_callback, hook_callback_data)
